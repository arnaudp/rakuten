package com.priceminister.account;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.priceminister.account.exception.IllegalAmountException;
import com.priceminister.account.exception.IllegalBalanceException;
import com.priceminister.account.exception.IllegalRuleException;
import com.priceminister.account.implementation.CustomerAccount;
import com.priceminister.account.implementation.CustomerAccountRule;


/**
 * Please create the business code, starting from the unit tests below.
 * Implement the first test, the develop the code that makes it pass.
 * Then focus on the second test, and so on.
 * 
 * We want to see how you "think code", and how you organize and structure a simple application.
 * 
 * When you are done, please zip the whole project (incl. source-code) and send it to recrutement-dev@priceminister.com
 * 
 */
public class CustomerAccountTest {
    
    private Account customerAccount;
    private AccountRule rule;

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();
    
    /**
     * Init of the tests
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        customerAccount = new CustomerAccount();
    	rule = new CustomerAccountRule();
    }
    
    /**
     * Tests that an empty account always has a balance of 0.0, not a NULL.
     */
    @Test
    public void testAccountWithoutMoneyHasZeroBalance() {
    	// check that balance exists
    	assertNotNull("Balance must exists", customerAccount.getBalance());
    	
    	// check balance of an empty account is always 0.0 
    	assertTrue("Balance of empty account must be zero", customerAccount.getBalance().compareTo(0d) == 0);
    }
    
    /**
     * Case passing : Adds money to the account and checks that the new balance is as expected.
     * @throws IllegalAmountException 
     */
    @Test
    public void testAddPositiveAmount() throws IllegalAmountException {
    	// get the actual balance
    	double actualBalance = customerAccount.getBalance();
    	
    	// add 10 euros
    	double amount10 = 10d;
    	customerAccount.add(amount10);
    	
    	assertTrue("Account balance " + customerAccount.getBalance() + " is not equal with " + actualBalance + " +" + amount10,customerAccount.getBalance().equals(actualBalance+amount10));
    	
    	// add 20 euros
    	double amount20 = 20d;
    	actualBalance = customerAccount.getBalance();
    	customerAccount.add(amount20);
    	
    	assertTrue("Account balance " + customerAccount.getBalance() + " is not equal with " + actualBalance + " +" + amount20,customerAccount.getBalance().equals(actualBalance+amount20));
    }
    
    
    /**
     * Error case "negative" : Adds negative money to the account and checks that the errors messages are appropriated with the case.
     * @throws IllegalAmountException 
     */
    @Test
    public void testAddInvalidAmount() throws IllegalAmountException {

    	exceptionRule.expect(IllegalAmountException.class);

    	// add negative value : -30 euros
    	exceptionRule.expectMessage("Amount is negative");
    	double amountNegative30 = -30d;
    	customerAccount.add(amountNegative30);
    	
    	// add nullable value
    	exceptionRule.expectMessage("Amount is null");
    	customerAccount.add(null);
    }
    
    
    /**
     * Case passing : Tests that after a legal withdrawal, the account balance is correct
     * @throws IllegalBalanceException 
     * @throws IllegalAmountException 
     * @throws IllegalRuleException 
     */
    @Test
    public void testWithdrawAndReportBalance() throws IllegalBalanceException,IllegalAmountException, IllegalRuleException{
    	customerAccount.add(100d);

    	double balance = customerAccount.getBalance();
    	
    	double withdrawnAmount = 10d;
    	assertEquals("Account balance is not correct after the legal withdraw",customerAccount.withdrawAndReportBalance(withdrawnAmount, rule),balance-withdrawnAmount,0d);
    }

    /**
     * Error case "null" : Tests that an illegal withdrawal throws the expected exception.
     * @throws IllegalBalanceException 
     * @throws IllegalAmountException 
     * @throws IllegalRuleException 
     */
    @Test(expected = IllegalAmountException.class)
    public void testWithdrawAndReportBalanceWithNullAmount() throws IllegalBalanceException,IllegalAmountException, IllegalRuleException{
    	customerAccount.withdrawAndReportBalance(null, rule);
    }
    
    /**
     * Error case "negative" : Tests that an illegal withdrawal throws the expected exception.
     * @throws IllegalBalanceException 
     * @throws IllegalAmountException 
     * @throws IllegalRuleException 
     */
    @Test(expected = IllegalAmountException.class)
    public void testWithdrawAndReportBalanceWithNegativeAmount() throws IllegalBalanceException,IllegalAmountException, IllegalRuleException{
    	double withdrawnAmount = -10d;
    	customerAccount.withdrawAndReportBalance(withdrawnAmount, rule);
    }
    
    /**
     * Error case "zero" : Tests that an illegal withdrawal throws the expected exception.
     * Use the logic contained in CustomerAccountRule; feel free to refactor the existing code.
     * @throws IllegalBalanceException 
     * @throws IllegalAmountException 
     * @throws IllegalRuleException 
     */
    @Test(expected = IllegalAmountException.class)
    public void testWithdrawAndReportBalanceWithZeroAmount() throws IllegalBalanceException,IllegalAmountException, IllegalRuleException{
    	double withdrawnAmount = 0d;
    	customerAccount.withdrawAndReportBalance(withdrawnAmount, rule);
    }

}
