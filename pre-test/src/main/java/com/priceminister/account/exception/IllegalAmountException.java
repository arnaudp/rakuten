package com.priceminister.account.exception;

public class IllegalAmountException extends Exception{
	private static final long serialVersionUID = 7060865002585795417L;

	public IllegalAmountException(String errorMessage) {
		super(errorMessage);
	}
}