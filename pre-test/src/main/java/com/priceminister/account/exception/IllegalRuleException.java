package com.priceminister.account.exception;

public class IllegalRuleException extends Exception {
	private static final long serialVersionUID = 3057246281544327525L;

	public IllegalRuleException(String errorMessage) {
		super(errorMessage);
	}
}
