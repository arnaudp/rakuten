package com.priceminister.account.implementation;

import com.priceminister.account.Account;
import com.priceminister.account.AccountRule;
import com.priceminister.account.exception.IllegalAmountException;
import com.priceminister.account.exception.IllegalBalanceException;
import com.priceminister.account.exception.IllegalRuleException;

public class CustomerAccount implements Account {

	private Double balance;
	
    public CustomerAccount() {
		this.balance = 0d;
	}
    
    public Double getBalance() {
    	return balance;
    }

	public void add(Double addedAmount) throws IllegalAmountException {
		// check null value
		if(addedAmount == null) { 
			throw new IllegalAmountException("Amount is null");
		} 
		// check negative value		
		else if(addedAmount.compareTo(0d) < 0) {
			throw new IllegalAmountException("Amount is negative");
		}
		balance += addedAmount;
    }

    public Double withdrawAndReportBalance(Double withdrawnAmount, AccountRule rule) throws IllegalBalanceException, IllegalAmountException, IllegalRuleException{
		// check null value for amount
    	if(withdrawnAmount == null) {
    		throw new IllegalAmountException("Withdrawn amount is null");
    	}
		// check negative or zero value		
    	else if(withdrawnAmount <= 0) {
    		throw new IllegalAmountException("Withdrawn amount is negative or zero");
    	}
    	// check null value for rule
    	else if(rule == null) {
    		throw new IllegalRuleException("Rule is not defined");
    	}
		// positive value		
    	else {
    		// simulate the previsionnal balance to check it after
    		double previsionnalBalance = balance-withdrawnAmount;
    		boolean withdrawPermitted = rule.withdrawPermitted(previsionnalBalance);
    		
    		if(withdrawPermitted) {
    			balance = previsionnalBalance;
    		}else {
    			throw new IllegalBalanceException(previsionnalBalance);
    		}
    	}
    	return balance;
    }
}
